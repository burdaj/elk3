Vf oscilátory
=============
Jiří Burda
duben 2020
:stem: latexmath

stem:[a^2+b^2=c^2]

Oscilátor je obvod, který *vyrábí harmonické (sinusové) napětí* určitých parametrů (velikosti, frekvence,...). Jedním z principů jak oscilátor realizovat je vytvoření *kladné zpětné vazby* v zesilovači takové velikosti, že zesilovač přestane být stabilní a rozkmitá se. Připomeňme zapojení zesilovače s kladnou zpětnou vazbou.

image:images/Ideal_feedback_model.svg[Zpětná vazba]

====
Aby se obvod rozkmital tak musí platit *amplitudová podmínka* vzniku oscilací: stem:[\beta \cdot A > 1] a *fázová podmínka* vzniku oscilací =0, 2,... Zesilovač zabezpečuje amplitudovou podmínku a může být realizován tranzistorem nebo lze použít OZ. Fázová podmínka, která určuje frekvenci kmitů, se definuje pomocí pasivního obvodu v kladné zpětné vazbě. Pro oblast vf tj. rádio > 100 kHz, se jako pasivní obvody používaly LC obvody především rezonanční obvody sériový a paralelní. Nyní se k řízení oscilací používá krystal, který má přesněji definovanou a stabilnější rezonanční frekvenci.
====

stem:[\sqrt(4) = 2]
